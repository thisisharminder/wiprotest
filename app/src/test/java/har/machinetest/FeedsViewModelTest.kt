package har.machinetest

import har.machinetest.retrofit.Apis
import har.machinetest.retrofit.RetrofitModule
import har.machinetest.ui.feeds.FeedsViewModel
import har.machinetest.data.pojo.PojoFeeds
import io.reactivex.observers.TestObserver
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Inject


@RunWith(MockitoJUnitRunner::class)
class FeedsViewModelTest {

    @Inject
    lateinit var apis: Apis

    private var feedsViewModel: FeedsViewModel? = null
    lateinit var testComponent: TestComponent

    @Mock
    internal var app: BaseApp? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        initDI()
        feedsViewModel = FeedsViewModel(app!!)

    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        feedsViewModel = null
    }

    @Test
    fun testServerLoginSuccess() {
        val testObserver: TestObserver<PojoFeeds> = TestObserver()
        val observable = apis.getFeeds()
        observable.subscribe(testObserver)
        if (testObserver.valueCount() > 0) {
            Assert.assertTrue(!testObserver.values().isEmpty()) //Assuming list is not empty for pass case (can change the condition)
        } else {
            val mThrowable = testObserver.errors()[0]
            mThrowable?.let {
                Assert.fail(it.message)
            } ?: run { Assert.fail("No Error Message") }

        }
    }


    private fun initDI() {
        testComponent = DaggerTestComponent.builder()
            .retrofitModule(RetrofitModule())
            .build()
        testComponent.inject(this)
    }

}
