package har.machinetest

import dagger.Component
import har.machinetest.retrofit.RetrofitModule
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class])
interface TestComponent {
    fun inject(baseAPITest: FeedsViewModelTest)
}
