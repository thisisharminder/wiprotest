package har.machinetest.extension

import android.view.View
import android.webkit.URLUtil

fun unWraping(any: Any?): Any {
    val value = any.let {
        val value: Any
        when (any) {
            String, Double, Int -> {
                if (any.toString().isBlank()) value = ""
                else value = any.toString()
            }

            null -> value = ""
            else -> value = any.toString()
        }
        value
    }
    return value
}

fun checkUrlvalidation(imageHref: String?, view: View) {
    if (URLUtil.isValidUrl(imageHref)) view.visibility = View.VISIBLE
    else view.visibility = View.GONE
}

fun viewVisibility(data: String?, view: View): Int {
    if (data.isNullOrBlank()) view.visibility = View.GONE
    else view.visibility = View.VISIBLE
    return view.visibility
}