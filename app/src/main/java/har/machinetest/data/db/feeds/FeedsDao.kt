package har.machinetest.data.db.feeds

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import har.machinetest.data.pojo.PojoFeeds

@Dao
interface FeedsDao {
    @Query("SELECT * FROM $FEEDS_DATABASE")
    fun pojoFeeds(): PojoFeeds?

    @Query("SELECT COUNT(*) FROM $FEEDS_DATABASE")
    fun countFeeds(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg posts: PojoFeeds)

    @Query("DELETE FROM $FEEDS_DATABASE")
    fun nukeTable()
}