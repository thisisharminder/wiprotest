package har.machinetest.data.db.feeds

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import har.machinetest.data.pojo.FeedsData

class DataConverter {
    @TypeConverter
    fun fromFeeds(feedsData: List<FeedsData>?): String? {
        if (feedsData == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<FeedsData>>() {}.type
        return gson.toJson(feedsData, type)
    }

    @TypeConverter
    fun toFeedsList(feeds: String?): List<FeedsData>? {
        if (feeds == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<FeedsData>>() {}.type
        return gson.fromJson<List<FeedsData>>(feeds, type)
    }
}