package har.machinetest.data.db.feeds

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import har.machinetest.data.pojo.PojoFeeds

@Database(entities = arrayOf(PojoFeeds::class), version = 101)
abstract class FeedsDatabase : RoomDatabase() {
    abstract fun feedsDao(): FeedsDao
}