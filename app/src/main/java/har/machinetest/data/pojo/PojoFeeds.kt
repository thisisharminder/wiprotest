package har.machinetest.data.pojo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import har.machinetest.data.db.feeds.DataConverter
import har.machinetest.data.db.feeds.FEEDS_DATABASE

@Entity(tableName = FEEDS_DATABASE)
@TypeConverters(DataConverter::class)
data class PojoFeeds(@PrimaryKey(autoGenerate = true) var id: Int, var title: String? = "", var rows: List<FeedsData>?)

class FeedsData(@SerializedName("title") @Expose var title: String?, @SerializedName("description") @Expose var description: String?, @SerializedName(
    "imageHref") @Expose var imageHref: String?)

