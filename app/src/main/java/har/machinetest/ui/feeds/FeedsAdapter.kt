package har.machinetest.ui.feeds

import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import har.machinetest.R
import har.machinetest.data.pojo.FeedsData
import har.machinetest.extension.checkUrlvalidation
import har.machinetest.extension.unWraping
import har.machinetest.extension.viewVisibility
import kotlinx.android.synthetic.main.item_fact.view.*

/* Adapter to show feeds*/
class FeedsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MyViewHolder(inflater.inflate(R.layout.item_fact, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bindItems(feedsArrayList[position])
    }

    override fun getItemCount(): Int = feedsArrayList.size
    var feedsArrayList = ArrayList<FeedsData>()
    //adding elemnts to arraylist
    fun addItems(list: List<FeedsData>) {
        feedsArrayList.clear()
        feedsArrayList.addAll(list)
        notifyDataSetChanged()
    }

    // Clean all elements of the recycler
    fun clear() {
        feedsArrayList.clear();
        notifyDataSetChanged();
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(banner: FeedsData) {
            itemView.tvTitle.text = unWraping(banner.title).toString()
            itemView.tvDesc.text = unWraping(banner.description).toString()
            viewVisibility(banner.title, itemView.tvTitle)
            viewVisibility(banner.description, itemView.tvDesc)
            checkUrlvalidation(banner.imageHref, itemView.ivFact)
            // image loading & hiding imageview for broken image
            banner.imageHref?.apply {
                val options = RequestOptions().placeholder(android.R.drawable.ic_menu_myplaces)
                    .error(android.R.drawable.stat_notify_error)

                Glide.with(itemView.context).load(this).apply(options).listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?,
                                              model: Any?,
                                              target: Target<Drawable>?,
                                              isFirstResource: Boolean): Boolean {
                        itemView.ivFact.visibility = View.GONE
                        return true
                    }

                    override fun onResourceReady(resource: Drawable?,
                                                 model: Any?,
                                                 target: Target<Drawable>?,
                                                 dataSource: DataSource?,
                                                 isFirstResource: Boolean): Boolean {
                        itemView.ivFact.visibility = View.VISIBLE
                        itemView.ivFact.setImageDrawable(resource)
                        return true
                    }
                }).into(itemView.ivFact)
            }
        }
    }
}







