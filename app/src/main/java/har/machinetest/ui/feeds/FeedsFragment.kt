package har.machinetest.ui.feeds

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import har.machinetest.R
import har.machinetest.base.BaseFragment
import har.machinetest.retrofit.Apis
import kotlinx.android.synthetic.main.frag_feed.*
import kotlinx.android.synthetic.main.layout_no_item.*
import javax.inject.Inject

class FeedsFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {
    @Inject
    lateinit var viewModal: FeedsViewModel
    @Inject
    lateinit var adapter: FeedsAdapter
    @Inject
    lateinit var retrofit: Apis
    @Inject
    lateinit var layoutManager: LinearLayoutManager

    override fun getContentLayoutResId() = R.layout.frag_feed
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getData()
        setAdapter()
        setListener()
    }

    /* set listener- onclick, refresh etc.*/
    private fun setListener() {
        swipeContainer.setOnRefreshListener(this)
    }

    /* api call and setting the data*/
    private fun getData() {
        viewModal.liveFeeds.observe(this, Observer {
            swipeContainer.isRefreshing = false
            it?.apply {
                activity?.title = it.title
                if (rows.isNullOrEmpty()) {
                    clNoData.visibility = View.VISIBLE
                } else {
                    clNoData.visibility = View.GONE
                    adapter.addItems(it.rows!!)
                }
            }
        })

        viewModal.errorLiveData.observe(this, Observer {
            swipeContainer.isRefreshing = false
            clNoData.visibility = View.GONE
        })

        swipeContainer.isRefreshing = true
        viewModal.getFeeds()
    }

    /* setting adapter*/
    private fun setAdapter() {
        rvFeeds.layoutManager = layoutManager
        rvFeeds.adapter = adapter
    }

    /* on swipe to refresh*/
    override fun onRefresh() {
        viewModal.getFeeds()
    }
}
