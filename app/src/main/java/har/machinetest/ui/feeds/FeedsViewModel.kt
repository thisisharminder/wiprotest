package har.machinetest.ui.feeds

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.MutableLiveData
import har.machinetest.base.BaseViewModel
import har.machinetest.data.db.feeds.FeedsDao
import har.machinetest.data.pojo.PojoFeeds
import har.machinetest.extension.setObserver
import har.machinetest.retrofit.Apis
import har.machinetest.retrofit.CallbackWrapperRx
import io.reactivex.schedulers.Schedulers

@SuppressLint("CheckResult")
class FeedsViewModel(app: Application) : BaseViewModel(app) {
    var liveFeeds = MutableLiveData<PojoFeeds>()
    lateinit var feedsDao: FeedsDao
    lateinit var api: Apis

    fun getFeeds() {
        // check if data exists in DB
        if (feedsDao.countFeeds() > 0) liveFeeds.value = feedsDao.pojoFeeds()
        api.getFeeds().subscribeOn(Schedulers.newThread()).observeOn(Schedulers.io()).doOnNext { it ->
            // filtering null data
            it.rows = it.rows?.filter {
                !it.description.isNullOrBlank() && !it.title.isNullOrBlank() && !it.imageHref.isNullOrBlank()
            }
            // Inserting values in DB
            feedsDao.insertAll(it)
        }.setObserver().subscribeWith(object : CallbackWrapperRx<PojoFeeds>(this) {
            override fun onSuccess(t: PojoFeeds) {
                liveFeeds.value = t
            }
        })
    }
}

