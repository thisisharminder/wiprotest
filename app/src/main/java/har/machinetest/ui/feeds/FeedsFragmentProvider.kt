package har.machinetest.ui.feeds

import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module
abstract class FeedsFragmentProvider {
    @ContributesAndroidInjector(modules = arrayOf(FeedsFragmentModule::class))
    internal abstract fun provideBlogFragmentFactory(): FeedsFragment
}