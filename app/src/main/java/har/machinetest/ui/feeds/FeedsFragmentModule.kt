package har.machinetest.ui.feeds

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides
import har.machinetest.data.db.feeds.FeedsDatabase
import har.machinetest.retrofit.Apis

@Module
class FeedsFragmentModule {
    @Provides
    fun provideOpenSourceAdapter(): FeedsAdapter {
        return FeedsAdapter()
    }

    @Provides
    fun provideLayoutManager(fragment: Context): LinearLayoutManager {
        return LinearLayoutManager(fragment)
    }

    @Provides
    fun provideViewModal(fragment: FeedsFragment, database: FeedsDatabase, apis: Apis): FeedsViewModel {
        val viewModel = ViewModelProviders.of(fragment)[FeedsViewModel::class.java]

        viewModel.feedsDao = database.feedsDao()
        viewModel.api = apis
        return viewModel
    }
}
