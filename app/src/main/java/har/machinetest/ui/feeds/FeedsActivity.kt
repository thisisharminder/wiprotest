package har.machinetest.ui.feeds

import android.os.Bundle
import har.machinetest.R
import har.machinetest.base.BaseActivity
import har.machinetest.utils.FragmentUtil

class FeedsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feeds)
        FragmentUtil.replaceFragment(this, FeedsFragment(), R.id.flContainer, false, FragmentUtil.TRANSITION_NONE)
    }
}