package har.machinetest.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import har.machinetest.ui.feeds.FeedsActivity
import har.machinetest.ui.feeds.FeedsFragmentProvider

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [AndroidSupportInjectionModule::class, FeedsFragmentProvider::class])
    abstract fun bindInitialActivity(): FeedsActivity
}