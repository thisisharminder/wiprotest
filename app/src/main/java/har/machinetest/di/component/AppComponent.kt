package har.machinetest.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import har.machinetest.BaseApp
import har.machinetest.di.builder.ActivityBuilder
import har.machinetest.di.module.ContextModule
import har.machinetest.di.module.DbModule
import har.machinetest.retrofit.RetrofitModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class,
                             RetrofitModule::class,
                             ActivityBuilder::class,
                             ContextModule::class,
                             DbModule::class))
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: BaseApp)
}