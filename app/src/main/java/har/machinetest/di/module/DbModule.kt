package har.machinetest.di.module

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import har.machinetest.data.db.feeds.FEEDS_DATABASE
import har.machinetest.data.db.feeds.FeedsDatabase

@Module
class DbModule {
    @Provides
    fun providesDb(context: Context): FeedsDatabase {
        val db =
            Room.databaseBuilder(context, FeedsDatabase::class.java, FEEDS_DATABASE).allowMainThreadQueries().build()
        return db
    }
}