package har.machinetest.retrofit

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class RetrofitModule {
    val connectionTimeout: Long = 30000
    val readTimeout: Long = 30000
    val writeTimeout: Long = 30000
    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.connectTimeout(connectionTimeout, TimeUnit.MILLISECONDS)
        okHttpClient.readTimeout(readTimeout, TimeUnit.MILLISECONDS)
        okHttpClient.writeTimeout(writeTimeout, TimeUnit.MILLISECONDS)
        return okHttpClient.build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val retrofit =
            Retrofit.Builder().baseUrl(ApiConstants.BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).client(okHttpClient).build()

        return retrofit
    }

    @Singleton
    @Provides
    fun provideApis(retrofit: Retrofit): Apis {
        return retrofit.create(Apis::class.java)
    }
}


