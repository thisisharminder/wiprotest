package har.machinetest.retrofit

import har.machinetest.data.pojo.PojoFeeds
import io.reactivex.Observable
import retrofit2.http.GET

interface Apis {
    @GET(ApiConstants.FACT)
    fun getFeeds(): Observable<PojoFeeds>
}