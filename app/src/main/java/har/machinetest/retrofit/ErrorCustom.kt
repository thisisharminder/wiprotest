package har.machinetest.retrofit

class ErrorCustom() {
    constructor(statuscode: Int, message: String) : this() {
        this.statuscode = statuscode
        this.message = message
    }

    var statuscode: Int = 0
    var message = ""
    var type = ""
}
