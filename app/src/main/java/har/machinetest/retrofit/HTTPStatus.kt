package har.machinetest.retrofit

class HTTPStatus {
    companion object {
        val STATUS_SUCCESS201 = 201
        val STATUS_INVALID_TOKEN = 401
        val BAD_REQUEST = 400
        val STATUS_SUCCESS200 = 200
        val CUSTOMER_BLOCKED = 403
        val NOT_FOUND = 404
        val INTERNAL_SERVER_ERROR = 500
        val ACCOUNT_NOT_EXIST = 402
        val MY_ERROR: Int = 709
    }
}
