package har.machinetest.base

import java.io.Serializable

/* Base data modal for API calling */
open class BaseDataModel : Serializable {
    var statusCode: Int = 0
    var message = ""
    var type = ""
}





