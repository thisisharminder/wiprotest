package har.machinetest.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection

/**
 * BaseFragment required to be extends by other fragments
 */
abstract class BaseFragment : Fragment() {
    var baseView: View? = null
    @LayoutRes
    protected abstract fun getContentLayoutResId(): Int

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        if (baseView == null) {
            baseView = inflater.inflate(getContentLayoutResId(), container, false)
        }
        return baseView
    }

    fun showloading() {
        (activity as BaseActivity).showLoading("")
    }

    fun hideloading() {
        (activity as BaseActivity).hideLoading()
    }
}