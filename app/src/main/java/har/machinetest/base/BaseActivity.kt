package har.machinetest.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/* Base Activity to be extended by other Activity */
abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    /*Setting title of toolbar*/
    fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    /*show progress bar*/
    fun showLoading(message: String) {
        ProgressBarDialog.showProgressBar(this, "")
    }

    /*dissmiss progress bar*/
    fun hideLoading() {
        ProgressBarDialog.dismissProgressDialog()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }
}




