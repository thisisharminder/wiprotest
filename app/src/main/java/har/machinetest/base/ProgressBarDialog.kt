package har.machinetest.base

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import har.machinetest.R

object ProgressBarDialog {
    var dialog: Dialog? = null
    fun showProgressBar(activity: Context, title: String) {
        var title = title

        if (dialog != null && dialog!!.isShowing) return

        try {
            if ("".equals(title, ignoreCase = true)) {
                title = "Loading..."
            }
            dialog = Dialog(activity, R.style.AppTheme)
            dialog?.apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window!!.setBackgroundDrawableResource(android.R.color.transparent)
                val layoutParams = window!!.attributes
                layoutParams.dimAmount = .5f
                window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                setCancelable(false)
                setCanceledOnTouchOutside(false)
                setContentView(R.layout.progressbar_dialog)
                show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissProgressDialog() {
        try {
            dialog?.apply {
                if (isShowing) {
                    dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
