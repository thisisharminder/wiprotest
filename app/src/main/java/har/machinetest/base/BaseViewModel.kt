package har.machinetest.base

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import es.dmoral.toasty.Toasty
import har.machinetest.R
import har.machinetest.retrofit.ErrorCustom
import har.machinetest.retrofit.HTTPStatus

abstract class BaseViewModel(var app: Application) : AndroidViewModel(app), BaseView {
    var errorLiveData = MutableLiveData<ErrorCustom>()
    override fun isNetworkAvailable(): Boolean {
        return isNetworkAvailable()
    }

    override fun showLoading(message: String) {
        ProgressBarDialog.showProgressBar(app, "")
    }

    override fun hideLoading() {
        ProgressBarDialog.dismissProgressDialog()
    }

    override fun onUnknownError(error: String) {
        Toasty.error(app, error).show()
        hideLoading()
        errorLiveData.value = ErrorCustom(HTTPStatus.MY_ERROR, error)
    }

    override fun onTimeout() {
        Toasty.error(app, app.getString(R.string.timeout)).show()
        hideLoading()
        errorLiveData.value = ErrorCustom(HTTPStatus.MY_ERROR, app.getString(R.string.timeout))
    }

    override fun onNetworkError() {
        Toasty.error(app, app.getString(R.string.internet_connection)).show()
        hideLoading()
        errorLiveData.value = ErrorCustom(HTTPStatus.MY_ERROR, app.getString(R.string.internet_connection))
    }

    override fun onConnectionError() {
        Toasty.error(app, app.getString(R.string.timeout)).show()
        hideLoading()
    }

    fun throwError(error: ErrorCustom) {
        Toasty.error(app, error.message).show()
        errorLiveData.value = error
        hideLoading()
    }
}