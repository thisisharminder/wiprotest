package har.machinetest.base

/*  Base view for error handling and showing progress*/
interface BaseView {
    fun isNetworkAvailable(): Boolean
    fun onUnknownError(error: String)
    fun onTimeout()
    fun onNetworkError()
    fun onConnectionError()
    fun showLoading(message: String)
    fun hideLoading()
}