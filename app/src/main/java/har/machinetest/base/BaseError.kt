package har.machinetest.base

import har.machinetest.retrofit.ErrorCustom

/*  base Error to be used when there is need to process internal error provided by API maker */
interface BaseError {
    fun onError(error: ErrorCustom)
}