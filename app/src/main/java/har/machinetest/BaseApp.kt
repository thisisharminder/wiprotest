package har.machinetest

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import har.machinetest.di.component.AppComponent
import har.machinetest.di.component.DaggerAppComponent
import javax.inject.Inject

class BaseApp : Application(), HasActivityInjector {
    companion object {
        lateinit var appComponent: AppComponent
    }

    @Inject
    lateinit var dispatchActivityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        initDagger()
        super.onCreate()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchActivityInjector
    }
}