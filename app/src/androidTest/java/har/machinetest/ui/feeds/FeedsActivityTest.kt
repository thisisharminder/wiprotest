package har.machinetest.ui.feeds

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import har.machinetest.R
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
public class FeedsActivityTest {

    @get:Rule
    var mActivityRule = ActivityTestRule(FeedsActivity::class.java)


    @Before
    fun setUPFragment() {
        mActivityRule.activity
            .supportFragmentManager.beginTransaction()
    }

    @Test
    fun fragment_can_be_instantiated() {
        mActivityRule.activity.runOnUiThread {
            val feedsFragment = FeedsFragment()
        }
        // Then use Espresso to test the Fragment
        onView(withId(R.id.rvFeeds)).check(matches(isDisplayed()))
        onView(withId(R.id.tvTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.tvDesc)).check(matches(isDisplayed()))

    }

    @Test
    fun checkViewsDisplay() {

        onView(withId(R.id.flContainer))
            .check(matches(isDisplayed()))

    }


}