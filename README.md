# Android MVVM Architecture: Test App

This repository contains a test project that implements MVVM architecture using Dagger2, RxJava, Retrofit and Room.


#### The app has following packages:
1. **base**: It contains all the base classes like baseactivity,basefragment etc.
2. **di**: Dependency providing classes using Dagger2.
3. **ui**: View classes along with their corresponding ViewModel.
4. **extension**: It contains all extension classes that uses kotlin functional programming approach.
5. **utils**: it contains all Utility classes.
